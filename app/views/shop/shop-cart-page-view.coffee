View = require 'views/base/view'

module.exports = class ShopCartPageView extends View
  autoRender: true
  className: 'shop-cart-page'
  template: require './templates/shop-cart-page'
