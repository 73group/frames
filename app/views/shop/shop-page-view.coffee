View = require 'views/base/view'

module.exports = class ShopPageView extends View
  autoRender: true
  className: 'shop-page'
  template: require './templates/shop'
