PageView     = require 'views/base/page-view'
template = require './templates/editor'

module.exports = class EditorPageView extends PageView
  template: template
  autoRender : true
  className  : 'editor-page'

  canvas   : '#canvas'
  fills    : '#fills'
  borders  : '#borders'
  wait     : '#wait'
  waitIcon : '#wait-icon'
  
  events:
    'click #fills img'   : 'updateFill'
    'click #borders img' : 'updateBorder'

  context: null

  config:
    maxWidth: 580
    maxHeight: 580
    borderWidth: 50
    fillWidth: 50
    cornerRadius: 50

  images:
    fill: null
    photo: null
    border: null

  start: ->
    @context = canvas.getContext '2d'
    @setFill '/images/paspartu/images/1039.jpg'
    @setPhoto '/images/default-photo.png'
    @setBorder '/images/baget/images/017-V89.jpg'

  draw: =>
    if @images.fill? and @images.photo? and @images.border? and @images.border.width
      @resize()
      @drawFill()
      @drawPhoto()
      @drawBorders()
      @$wait.fadeOut 'fast'

  resize: ->
    #console.log @$canvas
    console.log 'b: ' + @$canvas.width()
    @config.maxWidth     = 580
    @config.maxHeight    = 580
    @config.borderWidth  = 50
    @config.fillWidth    = 50
    @config.cornerRadius = 50
    newWidth  = @images.photo.width  + @config.borderWidth * 2  + @config.fillWidth * 2
    newHeight = @images.photo.height + @config.borderWidth * 2  + @config.fillWidth * 2
    indexWidth  = @config.maxWidth / newWidth
    indexHeight = @config.maxHeight / newHeight
    index = Math.min indexWidth, indexHeight
    if index < 1
      @images.photo.width  *= index
      @config.borderWidth  *= index
      @config.fillWidth    *= index
      @images.photo.height *= index
      newWidth  = @config.maxWidth
      newHeight = @config.maxHeight
    @$canvas.width @images.photo.width + @config.borderWidth * 2 + @config.fillWidth * 2
    @$canvas.height @images.photo.height + @config.borderWidth * 2 + @config.fillWidth * 2
    tabHeight = $(window).height() - 150
    @$fills.height tabHeight
    @$borders.height tabHeight
    @$wait.width @$canvas.width()
    @$wait.height @$canvas.height()
    @$waitIcon.width @$canvas.width()
    @$waitIcon.height @$canvas.height()

  updateFill: (event) ->
    @$wait.fadeIn 'fast'
    @setFill $(event.currentTarget).attr 'data-pattern'

  updatePhoto: (event) ->
    @$wait.fadeIn 'fast'
    @setPhoto event.currentTarget.src

  updateBorder: (event) ->
    @$wait.fadeIn 'fast'
    @setBorder $(event.currentTarget).attr 'data-pattern'

  setFill: (filename) ->
    $(@images.fill).off 'load'
    @images.fill = new Image
    $(@images.fill).on 'load', @draw
    @images.fill.src = filename

  setPhoto: (filename) ->
    $(@images.photo).off 'load'
    @images.photo = new Image
    $(@images.photo).on 'load', @draw
    @images.photo.src = filename

  setBorder: (filename) ->
    $(@images.border).off 'load'
    @images.border = new Image
    $(@images.border).on 'load', @draw
    @images.border.src = filename

  drawFill: ->
    @context.restore()
    @context.save()
    @context.beginPath()
    @context.moveTo @config.borderWidth, @config.borderWidth
    @context.lineTo @$canvas.width() - @config.borderWidth, @config.borderWidth
    @context.lineTo @$canvas.width() - @config.borderWidth, @$canvas.height() - @config.borderWidth
    @context.lineTo @config.borderWidth, @$canvas.height() - @config.borderWidth
    @context.closePath()
    @context.clip()
    pattern = @context.createPattern @images.fill, 'repeat'
    @context.rect @config.borderWidth, @config.borderWidth, @$canvas.width()  - @config.borderWidth - @config.borderWidth,  @$canvas.height() - @config.borderWidth - @config.borderWidth
    @context.fillStyle = pattern
    @context.fill()
    # crop corners
    @context.restore()
    @context.save()
    @context.beginPath()
    @context.moveTo @config.borderWidth + @config.fillWidth, @config.borderWidth + @config.fillWidth + @config.cornerRadius
    @context.bezierCurveTo  @config.borderWidth + @config.fillWidth, @config.borderWidth + @config.fillWidth - 10, @config.borderWidth + @config.fillWidth - 10, @config.borderWidth + @config.fillWidth, @config.borderWidth + @config.fillWidth + @config.cornerRadius, @config.borderWidth + @config.fillWidth
    # @context.lineTo config.borderWidth + config.fillWidth + config.cornerRadius, config.borderWidth + config.fillWidth
    @context.lineTo @$canvas.width() - @config.borderWidth - @config.fillWidth - @config.cornerRadius, @config.borderWidth + @config.fillWidth
    @context.bezierCurveTo  @$canvas.width() - @config.borderWidth - @config.fillWidth + 10, @config.borderWidth + @config.fillWidth, @$canvas.width() - @config.borderWidth - @config.fillWidth, @config.borderWidth + @config.fillWidth - 10, @$canvas.width() - @config.borderWidth - @config.fillWidth, @config.borderWidth + @config.fillWidth + @config.cornerRadius
    # @context.lineTo @$canvas.width() - config.borderWidth - config.fillWidth, config.borderWidth + config.fillWidth + config.cornerRadius
    @context.lineTo @$canvas.width() - @config.borderWidth - @config.fillWidth, @$canvas.height() - @config.borderWidth - @config.fillWidth - @config.cornerRadius
    @context.bezierCurveTo  @$canvas.width()  - @config.borderWidth - @config.fillWidth, @$canvas.height() - @config.borderWidth - @config.fillWidth + 10, @$canvas.width()  - @config.borderWidth - @config.fillWidth + 10, @$canvas.height() - @config.borderWidth - @config.fillWidth, @$canvas.width()  - @config.borderWidth - @config.fillWidth - @config.cornerRadius, @$canvas.height() - @config.borderWidth - @config.fillWidth
    # @context.lineTo @$canvas.width() - config.borderWidth - config.fillWidth - config.cornerRadius, @$canvas.height() - config.borderWidth - config.fillWidth
    @context.lineTo @config.borderWidth + @config.fillWidth + @config.cornerRadius, @$canvas.height() - @config.borderWidth - @config.fillWidth
    @context.bezierCurveTo  @config.borderWidth + @config.fillWidth - 10, @$canvas.height() - @config.borderWidth - @config.fillWidth, @config.borderWidth + @config.fillWidth, @$canvas.height() - @config.borderWidth - @config.fillWidth + 10, @config.borderWidth + @config.fillWidth, @$canvas.height() - @config.borderWidth - @config.fillWidth - @config.cornerRadius
    # @context.lineTo config.borderWidth + config.fillWidth, @$canvas.height() - config.borderWidth - config.fillWidth - config.cornerRadius
    @context.closePath()
    @context.clip()

  drawPhoto: ->
    @context.drawImage @images.photo, @config.borderWidth + @config.fillWidth, @config.borderWidth + @config.fillWidth, @images.photo.width, @images.photo.height

  drawBorders: ->
    if @images.border.height isnt @config.borderWidth
      @images.border.width = @images.border.width * @config.borderWidth / @images.border.height
      @images.border.height = @config.borderWidth
    
    # right
    @context.restore()
    @context.save()
    @context.translate @$canvas.width(), 0
    @context.rotate @toRadians 90
    filledWidth = 0
    loop
      #console.log 'loop'
      @context.drawImage @images.border, filledWidth, 0, @images.border.width, @images.border.height
      filledWidth += @images.border.width
      break if filledWidth >= @$canvas.height()

    # left
    @context.restore()
    @context.save()
    # @context.scale 1, -1
    @context.translate 0, canvas.height
    @context.rotate @toRadians -90
    filledWidth = 0
    loop
      @context.drawImage @images.border, filledWidth, 0, @images.border.width, @images.border.height
      filledWidth += @images.border.width
      break if filledWidth >= canvas.height

    # top
    @context.restore()
    @context.save()
    @context.beginPath()
    @context.moveTo 0, 0
    @context.lineTo @config.borderWidth, @config.borderWidth
    @context.lineTo canvas.width - @config.borderWidth, @config.borderWidth
    @context.lineTo canvas.width, 0
    @context.closePath()
    @context.clip()
    filledWidth = 0
    loop
      @context.drawImage @images.border, filledWidth, 0, @images.border.width, @images.border.height
      filledWidth += @images.border.width
      break if filledWidth >= canvas.width

    # bottom
    @context.restore()
    @context.save()
    @context.translate canvas.width, canvas.height
    @context.rotate @toRadians 180
    @context.beginPath()
    @context.moveTo 0, 0
    @context.lineTo @config.borderWidth, @config.borderWidth
    @context.lineTo canvas.width - @config.borderWidth, @config.borderWidth
    @context.lineTo canvas.width, 0
    @context.closePath()
    @context.clip()
    filledWidth = 0
    loop
      @context.drawImage @images.border, filledWidth, 0, @images.border.width, @images.border.height
      filledWidth += @images.border.width
      break if filledWidth >= canvas.width

  toRadians: (degree) ->
    degree * Math.PI / 180

  render: ->
    super
    @$canvas   = @$el.find(@canvas)
    @context   = @$canvas.get(0).getContext '2d'
    @$wait     = @$el.find(@wait)
    @$waitIcon = @$el.find(@waitIcon)
    @$fills    = @$el.find(@fills)
    @$borders  = @$el.find(@borders)
    @setFill '/images/paspartu/images/1039.jpg'
    @setPhoto '/images/default-photo.png'
    @setBorder '/images/baget/images/017-V89.jpg'
