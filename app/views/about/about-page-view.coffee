View = require 'views/base/view'

module.exports = class AboutPageView extends View
  autoRender: true
  className: 'about-page'
  template: require './templates/about'
