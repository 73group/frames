View = require 'views/base/view'

module.exports = class ProfilePageView extends View
  autoRender: true
  className: 'profile-page'
  template: require './templates/profile'
