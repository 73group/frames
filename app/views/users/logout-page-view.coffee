View = require 'views/base/view'

module.exports = class LogoutPageView extends View
  autoRender: true
  className: 'logout-page'
  template: require './templates/logout'
