View = require 'views/base/view'

module.exports = class SignupPageView extends View
  autoRender: true
  className: 'signup-page'
  template: require './templates/signup'
