View = require 'views/base/view'

module.exports = class LoginPageView extends View
  autoRender: true
  className: 'login-page'
  template: require './templates/login'

  events:
    'submit #login': 'login'

  login: (event) ->
    event.preventDefault() if event
    console.log 'lo'