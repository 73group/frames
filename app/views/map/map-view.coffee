View     = require 'views/base/view'
template = require './templates/map'
mediator = require 'mediator'
config   = require 'config'

module.exports = class MapView extends View
  template: template

  listen:
    'sync collection': 'initMap'
    'map#showCity mediator': 'showCity'

  map: 'map'

  initMap: ->
    if Modernizr.geolocation
      ymaps.ready navigator.geolocation.getCurrentPosition @positionByGeolocation, @positionByIP
    else
      @positionByIP()

  positionByGeolocation: (position) =>
    center = [position.coords.latitude, position.coords.longitude]
    @renderMap center

  renderMap: (coords) =>
    center =
      center: coords
      zoom: 11
    
    @map = new ymaps.Map @map, center

    #@map.geoObjects.add new ymaps.Placemark coords,
    #  (balloonContentBody: 'Вы здесь')
    #  (preset: 'islands#blueDotIcon')

    @collection.each (shop) =>
      point = [
        shop.get 'latitude'
        shop.get 'longitude'
      ]
      title = '<b>' + shop.get('title') + '</b><br>'
      address = shop.get('address') + '<br>'
      description = shop.get('description') + '<br>'
      href = 'http://' + shop.get('subdomain') + '.' + config.domain
      link = '<a href="' + href + '" title="Перейти на сайт">' + href + '</a><br>'
      balloon =
        balloonContentBody: title + link + address + description
      icon =
        preset: 'islands#redDotIcon'
      @map.geoObjects.add new ymaps.Placemark point, balloon, icon

    #route
    #shop = @collection.at 0
    #ymaps.route([
    # coords
    # [(shop.get 'latitude'), (shop.get 'longitude')]
    #], {
    # mapStateAutoApply: true
    #}).then (route) =>
    # route.getPaths().options.set({
    #   balloonContenBodyLayout: ymaps.templateLayoutFactory.createClass('$[properties.humanJamsTime]'),
    #   strokeColor: '009900ff',
    #   opacity: 0.9
    # })
    # @map.geoObjects.add route

  positionByIP: (params) =>
    city = mediator.user.get 'city'
    @renderMap [
      city.latitude
      city.longitude
    ]

  showCity: (coords) ->
    @map.setCenter coords
    @map.setZoom 11