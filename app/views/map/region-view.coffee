template = require './templates/region'
View = require 'views/base/view'

module.exports = class RegionView extends View
  className: 'map-region'
  template: template

  events:
    'click .map-city': 'showCity'

  showCity: (event) ->
    @publishEvent 'map#showCity', [
      @$(event.target).attr 'data-latitude'
      @$(event.target).attr 'data-longitude'
    ]