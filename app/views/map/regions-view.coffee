CollectionView = require 'views/base/collection-view'
Region = require 'views/map/region-view'

module.exports = class RegionsView extends CollectionView
  className: 'map-regions'
  itemView: Region