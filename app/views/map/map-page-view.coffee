PageView     = require 'views/base/page-view'
template = require './templates/map-page'
config   = require 'config'

module.exports = class MapPageView extends PageView
  autoRender: true
  template: template
  regions:
    geo:     '.geo-container'
    regions: '.regions-container'
    map:     '.map-container'