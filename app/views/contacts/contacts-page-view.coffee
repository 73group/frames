View = require 'views/base/view'

module.exports = class ContactsPageView extends View
  autoRender: true
  className: 'contacts-page'
  template: require './templates/contacts'
