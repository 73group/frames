Controller = require 'controllers/base/controller'
LoginPageView = require 'views/users/login-page-view'
SignupPageView = require 'views/users/signup-page-view'
LogoutPageView = require 'views/users/logout-page-view'
ProfilePageView = require 'views/users/profile-page-view'

module.exports = class UsersController extends Controller

  login: ->
    @view = new LoginPageView region: 'main'

  logout: ->
    @view = new LogoutPageView region: 'main'

  signup: ->
    @view = new SignupPageView region: 'main'

  profile: ->
    @view = new ProfilePageView region: 'main'