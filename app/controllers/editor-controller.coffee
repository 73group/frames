Controller     = require 'controllers/base/controller'
EditorPageView = require 'views/editor/editor-page-view'

module.exports = class EditorController extends Controller

  index: ->
    @view = new EditorPageView