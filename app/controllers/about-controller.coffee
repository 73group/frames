Controller = require 'controllers/base/controller'
AboutPageView = require 'views/about/about-page-view'

module.exports = class AboutController extends Controller

  index: ->
    @view = new AboutPageView region: 'main'