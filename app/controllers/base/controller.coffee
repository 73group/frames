mediator = require 'mediator'
SiteView = require 'views/site-view'
HeaderView = require 'views/header-view'

module.exports = class Controller extends Chaplin.Controller
  # Reusabilities persist stuff between controllers.
  # You may also persist models etc.
  beforeAction: (params, route) ->
    @reuse 'site', SiteView
    @reuse 'header', HeaderView