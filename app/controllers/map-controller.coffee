Controller  = require 'controllers/base/controller'
MapPageView = require 'views/map/map-page-view'
GeoView     = require 'views/map/geo-view'
MapView     = require 'views/map/map-view'
RegionsView = require 'views/map/regions-view'
Collection  = require 'models/base/collection'
Region      = require 'models/region'
Shop        = require 'models/shop'
mediator    = require 'mediator'
config      = require 'config'

module.exports = class MapController extends Controller

  index: ->
    @view  = new MapPageView

    @regions = new Collection null, model: Region
    @regions.url = config.api + 'regions/cities/sites'
    @regions.fetch().then =>
      autoCity = mediator.user.get('geo').city
      destination = Infinity
      currentCity = {}
      @regions.each (region) =>
        _.each region.get('cities'), (city) =>
          d = Math.acos(Math.sin(autoCity.lat)*Math.sin(city.latitude) + Math.cos(autoCity.lat)*Math.cos(city.latitude)*Math.cos(city.longitude - autoCity.lon))
          if d < destination
            destination = d
            currentCity = city

      mediator.user.set 'city', currentCity

      @view.subview 'regions', new RegionsView collection: @regions, region: 'regions'
      @view.subview 'geo', new GeoView collection: @regions, region: 'geo'
      @view.subview('geo').render()
      @view.subview('regions').render()

      @shops = new Collection null, model: Shop
      @shops.url = config.api + 'regions/cities/shops'
      @view.subview 'map', new MapView collection: @shops, region: 'map'
      @shops.fetch().then =>
        @view.subview('map').render()