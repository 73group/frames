Controller = require 'controllers/base/controller'
ShopPageView = require 'views/shop/shop-page-view'
ShopCartPageView = require 'views/shop/shop-cart-page-view'

module.exports = class ShopController extends Controller

  index: ->
    @view = new ShopPageView region: 'main'

  cart: ->
    @view = new ShopCartPageView region: 'main'