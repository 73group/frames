Controller = require 'controllers/base/controller'
ContactsPageView = require 'views/contacts/contacts-page-view'

module.exports = class ContactsController extends Controller

  index: ->
    @view = new ContactsPageView region: 'main'