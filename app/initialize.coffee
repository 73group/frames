Application = require 'application'
routes = require 'routes'

# Initialize the application on DOM ready event.
$ ->
  new Application {
    title: 'Мир рамок',
    controllerSuffix: '-controller',
    routes
  }
