mediator = require 'mediator'

# The application object.
module.exports = class Application extends Chaplin.Application

  initMediator: ->
    mediator.createUser()
    super

  start: ->
    mediator.user.fetch().then =>
      super
    , =>
      mediator.removeUser()
      super
