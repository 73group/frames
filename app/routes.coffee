# Application routes.
module.exports = (match) ->

  match '', 'map#index'
  match 'about', 'about#index'
  match 'contacts', 'contacts#index'

  match 'map', 'map#index'

  match 'catalog', 'shop#index'
  match 'cart', 'shop#cart'

  match 'editor', 'editor#index'

  match 'login', 'users#login'
  match 'signup', 'users#signup'
  match 'logout', 'users#logout'
  match 'profile', 'users#profile'